import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import ImageUploading from "react-images-uploading";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import DeleteIcon from "@material-ui/icons/Delete";
import { useStyles } from "./css";

export default function AdvertisementContent() {
  const classes = useStyles();

  const [images, setImages] = React.useState([]);
  const maxNumber = 4;

  const onChange = (imageList, addUpdateIndex) => {
    // data for submit
    console.log(imageList, addUpdateIndex);
    setImages(imageList);
  };

  return (
    <div className={classes.root}>
      <div className="App">
        <ImageUploading
          multiple
          value={images}
          onChange={onChange}
          maxNumber={maxNumber}
          dataURLKey="data_url"
        >
          {({
            imageList,
            onImageUpload,
            onImageRemoveAll,
            onImageUpdate,
            onImageRemove,
            isDragging,
            dragProps,
            errors
          }) => (
            // write your building UI
            <div className="upload__image-wrapper">
              <div>
                {errors.maxNumber && (
                  <div style={{ backgroundColor: "#f2f2f2", padding: "20px" }}>
                    <span style={{ color: "red", fontSize: "20px" }}>
                      Number of selected images exceed maxNumber
                    </span>
                  </div>
                )}
                {errors.acceptType && (
                  <span>Your selected file type is not allow</span>
                )}
                {errors.maxFileSize && (
                  <span>Selected file size exceed maxFileSize</span>
                )}
                {errors.resolution && (
                  <span>
                    Selected file is not match your desired resolution
                  </span>
                )}
              </div>
              <button
                className={classes.uploadButton}
                style={isDragging ? { color: "red" } : undefined}
                onClick={onImageUpload}
                {...dragProps}
              >
                <CloudUploadIcon style={{ marginRight: "5px" }} /> Upload image
              </button>
              &nbsp;
              <button
                className={classes.removeAllButton}
                onClick={onImageRemoveAll}
              >
                <DeleteIcon style={{ marginRight: "5px" }} />
                Remove all images
              </button>
              <p style={{ color: "black", fontSize: "15px" }}>
                Images should be in <b>350px X 132px.</b>
              </p>
              <br />
              <br />
              <Grid container spacing={1}>
                {imageList.map((image, index) => (
                  <Grid item xs={6} sm={6} md={3} lg={3}>
                    <div
                      key={index}
                      className="image-item"
                      style={{
                        textAlign: "center"
                      }}
                    >
                      <img
                        src={image["data_url"]}
                        alt=""
                        width="100px"
                        height="100px"
                      />
                      <p>Image {index + 1}</p>
                      <div className="image-item__btn-wrapper">
                        <button
                          className={classes.updateButton}
                          onClick={() => onImageUpdate(index)}
                        >
                          Update
                        </button>
                        <button
                          className={classes.removeButton}
                          onClick={() => onImageRemove(index)}
                        >
                          Remove
                        </button>
                      </div>

                      <br />
                      <br />
                    </div>
                  </Grid>
                ))}
              </Grid>
            </div>
          )}
        </ImageUploading>
      </div>
    </div>
  );
}
