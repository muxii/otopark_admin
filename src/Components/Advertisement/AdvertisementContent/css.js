import { makeStyles, fade } from "@material-ui/core/styles";
import { borderColor } from "@material-ui/system";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: "20px",
    [theme.breakpoints.down("sm")]: {
      padding: "5px"
    }
  },
  grow: {
    flexGrow: 1
  },
  uploadButton: {
    backgroundColor: "#fff",
    border: "2px solid #00BBDC",
    borderRadius: "10px",
    color: "#00BBDC",
    fontSize: "17px",
    marginRight: "10px",
    outline: "0px",
    "&:hover": {
      background: "#00BBDC",
      color: "#fff",
      border: "2px solid #00BBDC"
    }
  },
  removeAllButton: {
    backgroundColor: "#fff",
    border: "2px solid #00BBDC",
    borderRadius: "10px",
    color: "#00BBDC",
    fontSize: "17px",
    marginRight: "10px",
    outline: "0px",
    "&:hover": {
      background: "red",
      color: "#fff",
      border: "2px solid red"
    }
  },
  updateButton: {
    backgroundColor: "#fff",
    border: "2px solid #00BBDC",
    borderRadius: "10px",
    color: "#00BBDC",
    fontSize: "15px",
    marginRight: "10px",
    outline: "0px",
    "&:hover": {
      background: "#00BBDC",
      color: "#fff",
      border: "2px solid #00BBDC"
    }
  },
  removeButton: {
    backgroundColor: "#fff",
    border: "2px solid #00BBDC",
    borderRadius: "10px",
    color: "#00BBDC",
    fontSize: "15px",
    marginRight: "10px",
    outline: "0px",
    "&:hover": {
      background: "red",
      color: "#fff",
      border: "2px solid red"
    }
  }
}));
